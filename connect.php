<?php
$server="localhost";
$user="root";
$pass="";
$dbname="test2";

$dsn="mysql:host=$server;dbname=$dbname";
try {
    $connect=new PDO($dsn,$user,$pass);
    $connect->exec("SET character_set_connection = 'utf8'");
    $connect->exec("SET NAMES 'UTF8'");
} catch (PDOException $e) {
    echo "no connect to db :".$e->getMessage();
}